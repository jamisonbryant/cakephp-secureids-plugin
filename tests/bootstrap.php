<?php

require(dirname(__DIR__) . '/vendor/autoload.php');
use Cake\Datasource\ConnectionManager;

if (getenv('TRAVIS')) {
    // Ensure default test connection is defined
    if (!getenv('DSN')) {
        die('DSN envvar not defined. Unable to configure test database connection.');
    }

    ConnectionManager::setConfig('test', [ 'url' => getenv('DSN') ]);
}
