<?php

namespace SecureIds\Test\TestCase\Model\Behavior;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use SecureIds\Model\Behavior\IdentifiableBehavior;

class IdentifiableBehaviorTest extends TestCase
{
    /**
     * @var array Fixture(s) used by test
     */
    public $fixtures = [ 'plugin.SecureIds.Articles' ];

    /**
     * @var \Cake\ORM\Table Table(s) used by test
     */
    private $table;

    /**
     * Sets up test
     */
    public function setUp()
    {
        // Get reference to articles table
        $this->table = TableRegistry::get('Articles');

        // Set up parent
        parent::setUp();
    }

    public function testGenerateValidBase64Id()
    {
        // Generate a base64 ID and test it
        $generator = new IdentifiableBehavior($this->table);
        $base64Id = $generator->generateBase64Id();

        // Length is 13
        $this->assertTrue(strlen($base64Id) === 10);

        // Doesn't contain /
        $this->assertFalse(strpos($base64Id, '/'));

        // Doesn't contain +
        $this->assertFalse(strpos($base64Id, '+'));

        // Doesn't contain =
        $this->assertFalse(strpos($base64Id, '='));
    }

    public function testGenerateValidUuid()
    {
        // Generate a base64 ID and test it
        $generator = new IdentifiableBehavior($this->table);
        $uuid = $generator->generateUuid();

        // Length is 36
        $this->assertTrue(strlen($uuid) === 36);

        // Matches UUID v4 format
        $v4Regex = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';
        $this->assertEquals(preg_match($v4Regex, $uuid), 1);
    }

//    public function testAvoidBase64IdCollision()
//    {
//        // Fetch first article from database
//        $oldArticle = $this->table->find('all')->first();
//
//        // Create new article with duplicate BID
//        $newArticle = $this->table->newEntity(['bid' => $oldArticle->bid]);
//        $entity = $this->table->save($newArticle);
//        $this->assertInstanceOf('Cake\ORM\Entity', $entity);
//
//        // Compare saved BID to known BID
//        $this->assertNotEquals($oldArticle->bid, $newArticle->bid);
//    }
//
//    public function testAvoidUuidCollision()
//    {
//        // Fetch first article from database
//        $oldArticle = $this->table->find('all')->first();
//
//        // Create new article with duplicate BID
//        $newArticle = $this->table->newEntity(['uuid' => $oldArticle->uuid]);
//        $entity = $this->table->save($newArticle);
//        $this->assertInstanceOf('Cake\ORM\Entity', $entity);
//
//        // Compare saved BID to known BID
//        $this->assertNotEquals($oldArticle->uuid, $newArticle->uuid);
//    }
}
