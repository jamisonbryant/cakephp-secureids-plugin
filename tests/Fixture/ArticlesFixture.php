<?php

namespace SecureIds\Test\Fixture;

use Cake\Datasource\ConnectionManager;
use Cake\TestSuite\Fixture\TestFixture;

class ArticlesFixture extends TestFixture
{
    /**
     * @var array Fields that define fixture
     */
    public $fields = [
        'id' => [ 'type' => 'integer' ],
        'bid' => [ 'type' => 'string', 'length' => 255, 'null' => false ],
        'uuid' => [ 'type' => 'string', 'length' => 255, 'null' => false ],
        'title' => [ 'type' => 'string', 'length' => 255, 'null' => false ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id']]
        ]
    ];

    /**
     * @var array Records that comprise fixture
     */
    public $records = [
        [
            'bid' => 'eHiglIFD0A',
            'uuid' => 'b53d0dee-49ce-4134-aa59-22672f',
            'title' => 'Test Article #1',
        ],
        [
            'bid' => 'cPvpJrJuYA',
            'uuid' => 'b5003397-faf2-4f07-bba3-04bf43',
            'title' => 'Test Article #2',
        ],
    ];
}
