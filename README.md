# CakePHP Secure IDs Plugin

A CakePHP plugin for using secure, non-incremental IDs for models.

## Introduction

### What are Secure IDs?
Secure IDs are a way to use non-incremental, non-guessable, randomly-generated unique identifiers to records in your
database. This solves a whole host of problems, from usability to security and beyond. For more information, check
out Tom Scott's great video [Will YouTube Ever Run Out of Video IDs?](https://www.youtube.com/watch?v=gocwRvLhDf8)

### What is CakePHP?
CakePHP is a rapid-development PHP web application framework that uses the MVC (Models/Views/Controllers) architecture.
It was first released in August 2005 and is now on version 3.0 and is more powerful than ever! One of CakePHP's great
features is that it supports extending its own functionality via the use of plugins, of which this is one.

For more information about CakePHP, visit their website at [CakePHP.org](http://cakephp.org).

## Installation

## Prerequisites
* PHP 7.x
* CakePHP 3.x

### Command-Line Installation
Using Composer is the recommended way of installing this plugin because it allows you to easily download the plugin and
its dependencies and keep them up-to-date and make use of Composer's awesome class auto-loader.

To install the plugin using Composer:

1. Open a terminal and `cd` to your CakePHP application's root directory.
2. Run `composer require jamisonbryant/cakephp-secureids-plugin` to download the plugin and its dependencies.
3. If you'd like, open `composer.json` for editing and modify the versioning schema for the plugin to your liking.

### Manual Installation
You can also install the plugin manually by clicking the "Download ZIP" button above and unzipping it into your CakePHP
application's `plugins/` directory, **however if you do this you will have to perform updates manually as well.**

If you're familiar with git you can also download the repository directly to your hard drive using `git clone`. This
allows you to stay on the bleeding-edge of the plugin's development, but be warned that it's called "bleeding-edge" for
a reason.

### Setup
To add the plugin to your application, activate the plugin in your application's bootstrap file:

```php
Plugin::load('SecureIds');
```

Or use the Cake shell:

```shell
./bin/cake plugin load SecureIds
```

**NOTE:** You must add the columns `bid` (for Base64 ID) and `uuid` (for UUID) to the table schemas of each model you
want to have secure IDs. You can do this easily by running the included migration (requires the 
[CakePHP Migrations plugin](https://github.com/cakephp/migrations)):

```bash
./bin/cake migrations migrate -p SecureIds
```

### Usage
Because the plugin's functionality is implemented as a Behavior, you need to apply the behavior (called the
`Identifiable` behavior) to each model you want to use it on. To do this, you need to create a table definition class
for each model that you want to be able to fetch secure IDs for:

```php
// File: src/Model/Table/ArticlesTable.php

namespace App\Model\Table;

use Cake\ORM\Table;

class ArticlesTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('SecureIds.Identifiable');
    }
}
```

The above code assumes that we are working with an `Article` model, obviously you should substitute your own model name.
Note: You may (read: probably will) have to create a table definition file for each model, because CakePHP does not do
this for you by default, preferring instead to infer what your table definition is like on-the-fly. This is great for
most use cases, however when we want more control (to add a behavior, say), we need to create the files ourselves.

## Testing
You can test the plugin using CakePHP's built-in support for PHPUnit, however this assumes two things:

1. You have PHPUnit globally installed and the `phpunit` command available from the command-line
2. You didn't delete the `phpunit.xml.dist` that is created with new CakePHP 3.x applications

To test the plugin, first add its test suites to the application's PHPUnit config file:

```xml
<?xml version="1.0" encoding="UTF-8"?>

<phpunit ...>
    ...
    <testsuites>
        <testsuite name="App Test Suite">
            <directory>tests/TestCase</directory>
        </testsuite>

        <!-- Add these lines -->
        <testsuite name="Secure IDs Plugin Test Suite">
            <directory>vendor/jamisonbryant/cakephp-secureids-plugin/tests/TestCase</directory>
        </testsuite>
    </testsuites>
    ...
</phpunit>
```

Next, `cd` to your application's root directory and run `phpunit`.

If that doesn't work, run `composer dump-autoload` and try again.

## Contributing

### Bug Reports
If you find a bug in the plugin, or know of a missing feature you'd like to see added, feel free to create an issue on
the [Issues page](https://gitlab.com/jamisonbryant/cakephp-secureids-plugin/issues). **When reporting a bug, be as
descriptive as possible. Include what you were doing when the bug occurred, what might have caused it to occur, and how
to reproduce it if possible. Screenshots are nice, too.**

As a matter of courtesy, please use the search tool to verify that an issue has not already been reported before
creating a new issue. Feel free to add your comments/+1's to an open bug/feature request, but duplicate issues take time
to prune and crowd out new, important issues.

### Pull Requests
Found a bug you think you can fix? By all means go ahead! Clone the affected branch, fix the bug, then create a pull
request and I'll review and accept it as quickly as I can. However, before submitting a pull request, please be sure
that your code follows the [PSR-2 code style guidelines](https://www.php-fig.org/psr/psr-2). PRs that don't follow the
style guidelines will be rejected! (politely, of course)

## Legal

### License
Licensed under the MIT License (MIT). See `LICENSE.md` for details.

### Copyright
Copyright (c) 2019 Jamison Bryant. All rights reserved.
