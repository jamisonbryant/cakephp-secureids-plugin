<?php

namespace SecureIds\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use SecureIds\Generator\IdGenerator;

/**
 * SecureIds Shell
 *
 * Provides a CLI interface for using the Secure IDs plugin.
 *
 * @package SecureIds\Shell
 * @category  Shell
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2019 Jamison Bryant
 */
class SecureIdsShell extends Shell
{
    /**
     * @var array
     *
     * Array of Table objects with the Identifiable behavior attached
     */
    private $tables = [];

    /**
     * @var string
     *
     * Database connection to use (as defined in config/app.php)
     */
    private $conn = 'default';

    /**
     * @var bool
     *
     * Should records that already have IDs set be overwritten?
     */
    private $overwrite = false;

    /**
     * Define options for the shell
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        // Add options
        $parser->addOption('connection', [
            'short' => 'c',
            'help' => 'Database connection to use',
            'default' => 'default'
        ]);

        $parser->addOption('overwrite', [
            'short' => 'o',
            'help' => 'Overwrite IDs of records that already have them',
            'boolean' => true
        ]);

        // Set description and add subcommands
        $parser
            ->setDescription('Performs various tasks related to the SecureIDs plugin')
            ->addSubcommand('generate', [
                'help' => 'Generate secure IDs for existing records',
                'parser' => $parser
            ]);

        return $parser;
    }

    /**
     * Scans for tables the shell can work with
     */
    private function scanTables()
    {
        // Get list of tables
        $db = ConnectionManager::get($this->conn);
        $schema = $db->getSchemaCollection();
        $tables = $schema->listTables();

        // For each table in the schema, determine if it has the Identifiable behavior
        // Do this by loading the model for the table and checking the attached behaviors
        $this->out('Scanning for tables with Identifiable behavior...');

        foreach ($tables as $table) {
            // Load model for table
            $obj = TableRegistry::getTableLocator()->get($table);

            // Check if table has behavior
            // If so, add table object to tables property (used later)
            if ($obj->hasBehavior('Identifiable')) {
                $this->out(sprintf(' * Found table `%s`', $obj->getAlias()));
                $this->tables[] = $obj;
            }
        }

        $this->out(sprintf('Found %d table(s) with behavior.' . PHP_EOL, count($this->tables)));
    }

    /**
     * Generates secure IDs for each record in the table list
     */
    public function generate()
    {
        // Parse parameters
        $this->conn = $this->param('connection');
        $this->overwrite = $this->param('overwrite');

        // Scan for tables to work with
        $this->scanTables();

        foreach ($this->tables as $table) {
            $this->out(sprintf('Processing %s table...', $table->getAlias()));

            // Check if table has required columns
            $schema = $table->getSchema();
            $hasBidCol = !($schema->getColumn('bid') === null);
            $hasUuidCol = !($schema->getColumn('uuid') === null);
            $this->out(sprintf(' * Has Base64 ID column...%s', ($hasBidCol ? 'YES' : 'NO')));
            $this->out(sprintf(' * Has UUID column...%s', ($hasUuidCol ? 'YES' : 'NO')));

            if (!$hasBidCol && !$hasUuidCol) {
                $this->abort('ERROR: Table must have both `bid` and `uuid` columns! Aborting...');
            }

            // Check number of records
            $records = $table->find('all', ['fields' => ['id', 'bid', 'uuid']]);
            $this->out(sprintf(' * Found %d records', number_format($records->count())));

            // Update each record
            foreach ($records as $record) {
                // Check if Base64 ID/UUID are already set
                // If yes and --overwrite was not passed, abort.
                $bidSet = !empty($record->bid);
                $uuidSet = !empty($record->uuid);

                if ($bidSet || $uuidSet) {
                    if ($this->overwrite) {
                        $this->warn(sprintf('     - Record %s.%d already has secure IDs...overwriting...', $table->getAlias(), $record->id));
                    } else {
                        $this->abort(sprintf('     - Record %s.%d already has secure IDs and --overwrite is not set! Aborting...', $table->getAlias(), $record->id));
                    }
                }

                // Generate secure IDs
                $bid = IdGenerator::generateBase64Id();
                $uuid = IdGenerator::generateUuid();

                // Apply secure IDs to entity
                $record->bid = $bid;
                $record->uuid = $uuid;

                // Save changes to table
                $table->save($record);
            }

            $this->out(sprintf('Finished processing %s table.' . PHP_EOL, $table->getAlias()));
        }
    }
}