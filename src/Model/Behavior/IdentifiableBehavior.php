<?php

namespace SecureIds\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use SecureIds\Generator\IdGenerator;

/**
 * Identifiable Behavior
 *
 * This behavior is responsible for generating the Base64 IDs and UUIDs for records in tables it is attached to.
 *
 * To use this behavior on your table(s), add the following code to the table class:
 *
 * ```php
 * public function initialize(array $config)
 * {
 *     $this->addBehavior('SecureIds.Identifiable');
 *  }
 * ```
 * _(note: the table must have the `bid` and `uuid` columns -- see README.md for details)_
 *
 * **To generate secure IDs for existing records, run the migrations included with the plugin.**
 *
 * @package   SecureIds\Model\Behavior
 * @category  Behavior
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2019 Jamison Bryant
 */
class IdentifiableBehavior extends Behavior
{
    protected $_defaultConfig = [
        'base64' => [
            'field' => 'bid'
        ],
        'uuid' => [
            'field' => 'uuid'
        ],
    ];

    /**
     * Initializes the behavior and its parent
     *
     * @param array $config Array of configuration settings
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    /**
     * Generates IDs for an entity before it is saved to the database.
     *
     * @param Event $event Instance of save event
     * @param EntityInterface $entity Entity being saved
     */
    public function beforeSave(Event $event, EntityInterface $entity)
    {
        // Check if entity is being created in database
        // If so, update appropriate ID fields if present
        if ($entity->isNew()) {
            $base64Field = $this->getConfig('base64.field');
            $uuidField = $this->getConfig('uuid.field');

            // Get model/table from save event
            $table = $event->getSubject();

            do {
                // Generate Base64 ID/UUID
                // Don't overwrite a user-set BID/UUID unless there is a collision
                $setBase64Id = $entity->get($base64Field);
                $setUuid = $entity->get($uuidField);
                $newBase64Id = $setBase64Id ?? IdGenerator::generateBase64Id();
                $newUuid = $setUuid ?? IdGenerator::generateUuid();

                // Check for BID/UUID collisions
                $dupes = $table->findAllByBidOrUuid($newBase64Id, $newUuid);
                $entity->set($base64Field, null);
                $entity->set($uuidField, null);
            } while ($dupes->count() > 0);

            $entity->set($base64Field, $newBase64Id);
            $entity->set($uuidField, $newUuid);
        }
    }
}
