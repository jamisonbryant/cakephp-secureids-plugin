<?php

use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

/**
 * Add ID Columns Migrations
 *
 * Adds the `bid` and `uuid` columns to each table configured with the Identifiable behavior.
 *
 * To run this migration, run this command:
 *
 * ```shell
 * ./bin/cake migrations migrate -p SecureIds
 * ```
 *
 * @category  Migration
 * @author    Jamison Bryant <jamison@bryant.ai>
 * @copyright 2019 Jamison Bryant
 */
class AddIdColumns extends AbstractMigration
{
    /**
     * @var array
     *
     * Array of Table objects with the Identifiable behavior attached
     */
    protected $tables = [];

    /**
     * Initializes the migration
     *
     * Determines which tables have
     */
    public function init()
    {
        // Initialize parent
        parent::init();

        // Get list of tables
        $db = ConnectionManager::get('default');
        $schema = $db->getSchemaCollection();
        $tables = $schema->listTables();

        // For each table in the schema, determine if it has the Identifiable behavior
        // Do this by loading the model for the table and checking the attached behaviors
        foreach ($tables as $table) {
            // Load model for table
            $obj = TableRegistry::getTableLocator()->get($table);

            // Check if table has behavior
            // If so, add table object to tables property (used later)
            if ($obj->hasBehavior('Identifiable')) {
                $this->tables[] = $obj;
            }
        }
    }

    /**
     * Adds columns for Base64 ID (BID) and UUID
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $table) {
            // Get table data
            $alias = $table->getAlias();
            $table = $this->table($alias);

            // Update table definition
            $table
                ->addColumn('bid', 'string', [
                    'after' => 'id',
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ])
                ->addColumn('uuid', 'string', [
                    'after' => 'bid',
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ])
                ->update();
        }
    }

    /**
     * Removes added ID columns
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            // Get table data
            $alias = $table->getAlias();
            $table = $this->table($alias);

            // Update table definition
            $table
                ->removeColumn('bid')
                ->removeColumn('uuid')
                ->update();
        }
    }
}